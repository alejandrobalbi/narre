import {m} from 'motor'

export default class ComandoEvento extends m.Comando {
  get origen() { return this._origen}
  get descripcion() { return this._descripcion}
  get duracion() { return this._duracion}
  get desvio() { return this._desvio}
  get comandos() { return this._comandos}

  callback(src = {}) {
    let origen = this.origen
    if(origen !== undefined && src.resultados[origen] !== undefined) {
      origen = src.resultados[origen].fecha.clone()
    }
    if(origen === undefined && src.ultimo !== undefined) origen = src.ultimo.clone()
    if(origen === undefined && src.origen !== undefined) origen = src.origen.clone()
    let args = {
      descripcion: this.descripcion,
      origen: origen,
      duracion: this.duracion,
      desvio: this.desvio,
      comandos: this.comandos,
    }
    let evt = m.Evento.crear(args)
    src.ultimo = evt.hasta.clone()
    return evt
  }

  static crear( args = {} ) { return new ComandoEvento(args)}

}

m.ComandoEvento = ComandoEvento