import {m} from 'motor'
import moment from 'moment'

export default class Entorno extends m.Base {
  constructor(args = {} ) {
    super(args)
    this._eventos = this.eventos.map( e => new m.Evento(e) )
    this._personas = Object.fromEntries(Object.entries(this.personas).map( e => [e[0], new m.Persona(e[1])]))
  }

  get origen() {
    if( this._origen === undefined ) this._origen = moment(m.config().origen) 
    return this._origen
  }
  set origen(val) { this._origen = val}


  get personas() {
    if(this._personas === undefined) this._personas = {}
    return this._personas
  }
  get eventos() {
    if(this._eventos === undefined) this._eventos = []
    return this._eventos
  }

  persona(args = {}) {
    let persona = Object.values(this.personas).find( p => {
      if(p === undefined) return false
      if(p.nombre === args.nombre) return true
    })
    return persona
  }
  
  traer(args) { return m.registro().traer(args) }
  
  agregar(args) {
    if(args instanceof m.Evento) {
      let evt = args
      this.eventos.push(evt)
    } else if(args instanceof m.Persona) {
      let per = args
      this.personas[per.key] = per
    } else {
      throw(new Error('Se esperaba un Evento o una Persona'))
    }
  }

  to_obj() { return m.util.to_obj({...this}) }

  tabla_personas() {
    return Object.values(this.personas).map( p => {
      return {
        id: p.id,
        nombre: p.nombre,
        eventos: p.eventos.map( e => e.id)
      }
    })
  }
  
  tabla_eventos() {
    let a_desde, a_hasta, a_origen, desde, hasta, origen
    return this.eventos
    .sort((a,b) => {
      if(a.desde.isBefore(b.desde)) return -1
      if(a.desde.isAfter(b.desde)) return 1
      return 0
    } )
    .map( e => {
      desde = e.desde.format()
      hasta = e.hasta.format()
      origen = e.origen.format()
      let format = 'YYYY-MM-DDTHH:mm:ss'
      let args = { 
        id: e.id,
        "id / key / descripcion": e.descripcion ? e.descripcion : e.key, 
        origen: Entorno.diff(a_origen,e.origen.format(format)),
        desvio: e.desvio, 
        desde: Entorno.diff(a_desde,e.desde.format(format)),
        duracion: e.duracion, 
        hasta: Entorno.diff(a_hasta,e.hasta.format(format)),
      }
      a_desde = desde
      a_hasta = hasta
      a_origen = origen
      return args
    })    

  }

  static diff(anterior = '', nuevo) {
    let diff = []
    let arr_anterior = anterior.split(/[T]/)
    let arr_nuevo = nuevo.split(/[T]/)
    let c = 0
    for (const iterator of arr_anterior) {
      if(arr_anterior[c] !== arr_nuevo[c]) diff.push(arr_nuevo[c])
      c++
    }
    if(diff.length) return diff.join()
    return nuevo
  }
  static crear(args = {} ) { return new Entorno(args) }
  static destruir() { m.entorno = Entorno.crear() }
}

