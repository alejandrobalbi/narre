import {m} from 'motor'
import moment from 'moment'

export default class Persona extends m.Item {
  get nombre() { return this._nombre } 
  get eventos() {
    if(this._eventos === undefined) this._eventos = []
    return this._eventos
  }
  static crear(args ={} ) {
    let persona = new Persona(args)
    m.entorno.agregar(persona)
    m.registro().agregar(persona) 
    return persona 
  }
}

m.Persona = Persona