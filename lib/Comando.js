import {m} from 'motor'

export default class Comando extends m.Item {
  get callback() {
    if(this._callback === undefined) this._callback = false
    return this._callback
  }
  get ejecutado() {
    if(this._ejecutado === undefined) this._ejecutado = false
    return this._ejecutado
  }
  get resultado() { return this._resultado }

  ejecutar(evt) {
    let resultado
    if(typeof this.callback === 'function') {
      let res = evt ? evt.armar_resultados() : {}
      resultado = this.callback(evt, res)
      this._ejecutado = true
    } else {
      resultado = this.callback
      this._ejecutado = true
    }
    this._resultado = resultado
    return resultado
  }

  static ejecutar(args = {}) { return this.crear(args).ejecutar() }
  
  static crear(args = {}) { return new Comando(args) }

  static crear_evento(args = {}) {
    return Comando.crear(args)
  }

  static crear_evento(args = {}) {
    let evento = m.Evento.crear(args)
    m.entorno.eventos.push(evento)
    return evento
  }
}

m.Comando = Comando