import {m} from 'motor'
import moment from 'moment'

export default class Evento extends m.Item {
  get origen() { 
    return this._origen.clone()
  }
  get desvio() {
    if(this._desvio === undefined ) this._desvio = {} 
    return this._desvio
  }
  get duracion() {
    if(this._duracion === undefined ) this._duracion = {} 
    return this._duracion
  }
  get comandos() {
    if(this._comandos === undefined) this._comandos = []
    return this._comandos
  }
  get descripcion() {
    if(this._descripcion === undefined) this._descripcion = ''
    return this._descripcion
  }
  get ejecutado() {
    return this.comandos.map( (cmd) => cmd.ejecutado ).reduce((a, b) => a && b)
  }
  get resultados() { return this.armar_resultados() }

  get fecha() { return this.desde }
  get desde() { return this.origen.clone().add(this.desvio) }
  get hasta() { return this.origen.clone().add(this.desvio).add(this.duracion) }

  armar_resultados() {
    let res = []
    this.comandos.filter( cmd => {
      return cmd.resultado !== undefined
    }).map( cmd => res.push([cmd.key, cmd.resultado]))
    return Object.fromEntries(res)
  }

  agregar(args) {
    if(!(args instanceof m.Comando)) throw(new Error('Se esperaba un Comando'))
    let cmd = args
    this.comandos.push(cmd)
  }
  aplicar() {
    return this.comandos.map( (cmd) => {
      let res = cmd.ejecutar(this)
      if(res instanceof Evento) res.aplicar()
      return res
    } )
  }

  static crear(args ={} ) {
    if(args.origen === undefined) args.origen = m.entorno.origen.clone()
    let evento = new Evento(args)
    m.entorno.agregar(evento)
    m.registro().agregar(evento) 
    return evento 
  }
}

