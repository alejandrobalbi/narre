import {m} from 'motor'

export default class ComandoCrearPersona extends m.Comando {
  get nombre() { return this._nombre }
  get eventos() { return this._eventos }
  callback(src = {}) {
    let args = {
      key: this.key,
      nombre: this.nombre,
      eventos: this.eventos,
    }
    if(m.entorno.personas === undefined) m.entorno.personas = {}
    let personas = m.entorno.personas
    if(personas[args.key] === undefined) {
      personas[args.key] = args
    }
    args.eventos = args.eventos.map( (evt) => {
      let {key} = evt
      let evento = m.ComandoEvento.ejecutar(evt)
      return {key, id:evento.id}
    })
    let persona = m.Persona.crear(args)
    return persona
  }

  static crear( args = {} ) { 
    let {edad, nombre, eventos} = args
    if(eventos === undefined) eventos = []
    let key = m.util.generarKey(args.nombre)
    if(edad !== undefined ) {
      edad = parseInt(edad)
      let rng = m.rng()
      let desvio = {
        years: -edad,
        days: Math.round(rng.random() * 365) + 1,
        hours: Math.round(rng.random() * 24),
        minutes: Math.round(rng.random() * 60),
        seconds: Math.round(rng.random() * 60),
      }
      eventos = [...eventos, { desvio, descripcion:`Nace ${nombre}`, key:'nacimiento'}]
    }
    args = { key, nombre, eventos }    
    return new ComandoCrearPersona(args)
  }

}

m.ComandoCrearPersona = ComandoCrearPersona