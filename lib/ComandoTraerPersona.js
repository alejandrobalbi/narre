import { m } from 'motor'

export default class ComandoTraerPersona extends m.Comando {
  get nombre() { return this._nombre } 
  get key() { return this._key } 
  get id() { return this._id } 
  
  callback(src = {}) {
    let args = {
      nombre: this.nombre,
      key: this.key,
      id: this.id,
    }
    return m.entorno.persona(args)
  }

  static crear(args = {}) { return new ComandoTraerPersona(args) }
}

m.ComandoTraerPersona = ComandoTraerPersona