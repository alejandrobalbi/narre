const narre = {}
import {m} from 'motor'
import moment from 'moment'


import Entorno from "./lib/Entorno.js";
m.Entorno = Entorno
narre.Entorno = Entorno
m.entorno = Entorno.crear()

import Comando from "./lib/Comando.js";
narre.Comando = m.Comando

import ComandoEvento from "./lib/ComandoEvento.js";
narre.ComandoEvento = m.ComandoEvento

import ComandoCrearPersona from "./lib/ComandoCrearPersona.js";
narre.ComandoCrearPersona = m.ComandoCrearPersona

import ComandoTraerPersona from "./lib/ComandoTraerPersona.js";
narre.ComandoTraerPersona = m.ComandoTraerPersona

import Evento from "./lib/Evento.js";
m.Evento = Evento
narre.Evento = Evento

import Persona from "./lib/Persona.js";
m.Persona = Persona
narre.Persona = Persona


export const evento = (a1 = {}, a2 = {}) => {
  let args = a1
  if(typeof a1 === 'string') {
    args = { descripcion:a1, ...a2}
  }
  let evento = m.Evento.crear(args)
  return evento
}

export const comando_evento = (a1 = {}, a2 = {}) => {
  let args = a1
  if(typeof a1 === 'string') {
    args = { descripcion:a1, ...a2}
  }
  return m.ComandoEvento.crear(args)
}

export const crear_persona = (a1 = {}, a2 = {}) => {
  let args = a1
  if(typeof a1 === 'string') {
    args = { nombre:a1, ...a2}
  }
  return m.ComandoCrearPersona.crear(args)
}

export const estar = (a1 = {}, a2 = {}) => {
  let args = a1
  if(typeof a1 === 'string') {
    args = { nombre:a1, ...a2}
  }
  if(args.callback === undefined) {
    args.callback = (src = {}) => {
      let persona = m.ComandoTraerPersona.ejecutar(args)
      if(persona === undefined ) persona = m.ComandoCrearPersona.ejecutar(args)
      persona.eventos.push({key:src.key, id:src.id})
      return persona
    }
  }
  return m.Comando.crear(args)  
}


 

export {Entorno, narre}