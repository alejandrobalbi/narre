import {expect} from 'chai'
import {m} from 'motor'
import {narre} from '../index.js'

let evento
describe('Cuando tengo un evento', () => {
  beforeEach( () => evento = m.Evento.crear() )
  describe('y le agrego comandos', () => {
    beforeEach( () => {
      evento.agregar(m.Comando.crear())
      evento.agregar(m.Comando.crear())
      evento.agregar(m.Comando.crear())
    })
    it('entonces debo tener un comando mas en el evento', () => {
      expect(evento.comandos.length).to.be.eq(3)
    })
    describe('y aplico el evento', () => {
      beforeEach(() => evento.aplicar())
      it('entonces se deben marcar como ejecutados los comandos', () => {
        expect(evento.comandos.map( (cmd) => cmd.ejecutado ).reduce((a, b) => a && b)).to.be.eq(true)
      })
      it('entonces se deben marcar como ejecutado el evento', () => {
        expect(evento.ejecutado).to.be.eq(true)
      })
    })
  })
  describe('y le agrego comandos con callback y keys y lo aplico', () => {
    beforeEach( () => {
      evento.agregar(m.Comando.crear({key:'key1', callback:[1]}))
      evento.agregar(m.Comando.crear({key:'key2', callback: (evt, res) => [...res.key1, 2] }))
      evento.agregar(m.Comando.crear({key:'key3', callback: (evt, res) => [...res.key2, 3] }))
      evento.aplicar()
    })
    it('entoces puedo tener disponible los resultados de los comandos anteriores', () => {
      expect(evento.resultados.key1).to.be.eql([1])
      expect(evento.resultados.key2).to.be.eql([1,2])
      expect(evento.resultados.key3).to.be.eql([1,2,3])
    })
  })
})