import {expect} from 'chai'
import {m} from 'motor'
import {narre, crear_persona} from '../index.js'
import moment from 'moment'

let cmd, args, res, nacimiento

let epoca_victoriana = moment('1870-01-01','YYYY-MM-DD')

describe('Cuando creo un comando de crear_persona', () => {
  before( () => {
    let origen = m.config().origen
    m.config().origen = epoca_victoriana.format('X')    
    args = {
      nombre: 'Marguerite Voguel',
      edad: 23
    }
    cmd = crear_persona(args)
    res = cmd.ejecutar()
    nacimiento = res.eventos.find( e => e.key === 'nacimiento')
    m.config().origen = origen
  })
  it('entonces debe estar esa persona en el entorno', () => {
    expect(m.entorno.personas[res.key].nombre).to.be.eq(args.nombre) 
  })
  it('entonces debe tener un key que sea basado en el nombre', () => {
    expect(res.key).to.be.eq(m.util.generarKey(args.nombre))
  })
  it('entonces debe tener un evento nacimiento que esta en el entorno', () => {    
    expect(nacimiento.key).to.be.eq('nacimiento')
  })
})

