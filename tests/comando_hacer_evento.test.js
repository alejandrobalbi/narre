import {expect} from 'chai'
import {m} from 'motor'
import {narre, evento, comando_evento} from '../index.js'
import moment from 'moment'

let evt, cmd, args_comando, args_desvio, comando_evento1, comando_evento2, comando_evento3, comando_evento4, res
let origen = m.entorno.origen

describe("Si creo un evento", () => {
  beforeEach( () => {
    args_desvio = {days:12}
    evt = evento()
  })
  it('entonces obtengo un evento', () => { expect(evt).to.be.instanceOf(m.Evento) })
  it('entonces debo tener el origen de la configuracion', () => { expect(evt.origen).to.be.eql(origen) })
  it('entonces debo tener el desvio en 0', () => { expect(evt.desvio).to.be.eql({}) })
  it('entonces debo una fecha que sea igual al origen de la configuracion', () => { expect(evt.fecha).to.be.eql(origen) })
  describe("y le paso un desvio como un objeto con days, month, etc", () => {
    beforeEach( () => evt = evento({desvio:args_desvio}))
    it('entonces debo tener el desvio en correspondiente', () => {
      expect(evt.desvio).to.be.eql(args_desvio)
    })
  })
})


describe("Si creo un comando_evento", () => {
  beforeEach( () => {
    args_desvio = { y:1 }
    args_comando = {
      descripcion:"Esta es una descripcion",
      desvio: args_desvio
    }
    cmd = comando_evento(args_comando)
  })
  it('entonces obtengo un evento', () => expect(cmd).to.be.instanceOf(m.Comando))
  describe('y lo ejecuto', () => {
    beforeEach( () => evt = cmd.ejecutar())
    it('entonces obtengo un evento', () => { 
      let desvio = args_desvio
      expect(evt).to.be.instanceOf(m.Evento)
      expect(evt.descripcion).to.be.eql(args_comando.descripcion)
      expect(evt.desvio).to.be.eql(desvio)
      expect(m.entorno.eventos.find(e => e.id === evt.id).id).to.be.eq(evt.id)
    })
  })
})

describe('Cuando creo un evento que usa comando_evento', () => {
  beforeEach(() => {
    comando_evento1 = {key:'key1', descripcion: "Descripcion 1", desvio: {d:1} }
    comando_evento2 = {key:'key2', descripcion: "Descripcion 2", desvio: {d:1} }
    comando_evento3 = {key:'key3', descripcion: "Descripcion 3", desvio: {d:1}, origen:'key1'}
    comando_evento4 = {key:'key4', descripcion: "Descripcion 4", desvio: {d:1} }
    evt = evento({comandos: [
      comando_evento(comando_evento1),
      comando_evento(comando_evento2),
      comando_evento(comando_evento3),
      comando_evento(comando_evento4),
    ]})
  })
  it('entonces obtengo un evento', () => { 
    expect(evt).to.be.instanceOf(m.Evento)
    expect(evt.comandos[0]).to.be.instanceOf(m.Comando)
    expect(evt.comandos[0].key).to.be.eql(comando_evento1.key)
    expect(evt.comandos[1]).to.be.instanceOf(m.Comando)
    expect(evt.comandos[1].key).to.be.eql(comando_evento2.key)
  })
  describe('y lo aplico', () => {
    beforeEach( () => res = evt.aplicar())
    it('entonces los comando deben ser ejecutado', () => {
      expect(evt.comandos[0].ejecutado).to.be.eq(true)
      expect(evt.comandos[1].ejecutado).to.be.eq(true)
    })
    it('entonces la descripcion de los comandos debe ser la de los eventos generados', () => {
      expect(res[0].descripcion).to.be.eq(comando_evento1.descripcion)
      expect(res[1].descripcion).to.be.eq(comando_evento2.descripcion)
    })
    it('entonces los resultados del evento son accesibles via el key del comando', () => {
      expect(evt.resultados.key1.id).to.be.eq(res[0].id)
      expect(evt.resultados.key2.id).to.be.eq(res[1].id)
    })
    it('entonces en el entorno deben estar los eventos generados', () => {
      expect(m.entorno.eventos.find(e => e.id === res[0].id).descripcion).to.be.eq(res[0].descripcion )
      expect(m.entorno.eventos.find(e => e.id === res[1].id).descripcion).to.be.eq(res[1].descripcion )
    })
    it('entonces cada evento debe tener como origen la fecha de su predecesor', () => {
      expect(res[0].origen.format()).to.be.eq(origen.format())
      expect(res[1].origen.format()).to.be.eq(res[0].fecha.format())
      expect(res[3].origen.format()).to.be.eq(res[2].fecha.format())
    })
    describe('y indique un origen con el key del comando de otro evento anterior', () => {
      it('entonces el evento debe tener como origen la fecha del evento indicado en el key', () => {
        expect(res[2].origen.format()).to.be.eql(res[0].fecha.format())
      })
    })
  })
})