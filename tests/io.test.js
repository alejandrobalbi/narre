import {expect} from 'chai'
import {m} from 'motor'
import {narre, evento, comando_evento, crear_persona, estar} from '../index.js'
import moment from 'moment'


describe('Cuando tengo una implementacion de narre', () => {
  beforeEach( () => {
    m.Entorno.destruir()
    evento("El cuento", {origen: moment('976-01-01'), comandos:[
      comando_evento("Había una vez un valiente caballero llamado Joan", {comandos:[
        estar('Joan')
      ]})
    ]}).aplicar()
  })
  describe('y la convierto en objeto', () => {
    it('entonces puedo crear un nuevo entorno valido', () => {
      let args1 = m.entorno.to_obj()
      let entorno = m.Entorno.crear(args1)
      let args2 = entorno.to_obj()
      expect(args2).to.be.eql(args1)
    })  
  })
})