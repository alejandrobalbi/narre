import {expect} from 'chai'
import {m} from 'motor'
import {narre} from '../index.js'
import moment from 'moment'


let evento
describe('Cuando creo un Evento', () => {
  beforeEach( () => evento = m.Evento.crear() )
  it('entonces debe ser un Evento', () => {
    expect(evento).to.be.instanceOf(m.Evento)
  })
  it('entonces debe tener un id', () => {
    expect(evento.id).to.be.not.eq(undefined)
  })
  it('entonces debe tener un origen', () => {
    expect(evento.origen).to.be.eql(moment('1980-01-01'))
  })
  it('entonces debe tener un momento', () => {
    expect(evento.fecha.format()).to.be.eq(m.entorno.origen.format())
  })
  describe('y consulto fecha', () => {
    it('entonces debo obtener el origen', () => {
      expect( evento.fecha ).to.be.eql(evento.origen)
    })
  })
  describe('y agrego un desvio', () => {
    beforeEach( () => evento = m.Evento.crear({desvio:3600}))
    describe('y ejecuto timestamp', () => {
      it('entonces debo obtener el origen + desvio', () => {
        expect( evento.fecha.format() ).to.be.eq(evento.origen.clone().add(evento.desvio).format())
      })
    })
  })
})