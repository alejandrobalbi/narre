import {expect} from 'chai'
import {m} from 'motor'
import {narre, evento, comando_evento, crear_persona, estar} from '../index.js'
import moment from 'moment'




const str2objtime = (args) => {
    let obj = {}
  let str = args
  let rex = /(\d+)([smhdwMY])/
  while(str) {
    let match = str.match(rex)
    obj[match[2]] = parseInt(match[1])
    str = str.replace(rex, '')
    str = str.replace(' ', '')
  }
  return obj
}

const despues_de = (args) => { return {desvio:str2objtime(args)} }
const durante = (args) => { return {duracion:str2objtime(args)} }


describe('Cuando creo una historia', () => {
  before( () => {
    m.Entorno.destruir()
    m.entorno.origen = moment('1820-01-01')
    evento("El Zorro", {origen: moment('1820-01-01'), comandos:[
      estar('Alejandro de la Vega', {edad: 55}),
      estar('Don Nacho', {edad: 50}),
      estar('Diego de la Vega', {edad: 20}),
      estar('Bernardo', {edad: 40}),
      estar('Licenciado', {edad: 45}),
      estar('Monasterio', {edad: 40}),
      estar('Sargento Garcia', {edad: 40}),
      estar('Alcalde', {edad: 60}),
      estar('Pepito', {edad:11}),
      estar('Juan', {edad:65}),
      estar('Tornado', {edad:10}),
      comando_evento("Presentando al Zorro", {origen: moment('1820-01-01'), comandos: [
        comando_evento("Practica en la Cubierta", {...despues_de('8h'), ...durante('1m 20s'), comandos:[
          estar('Diego de la Vega'),estar('Capitan del Barco'),
        ]} ),
        comando_evento("Charla en el camarote", { desvio:{m:20}, duracion:{m:2,s:45}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),
        ]} ),
        comando_evento("Llegada a la plaza", { desvio:{h:2}, duracion:{m:1,s:35}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Sargento Garcia')
        ]} ),
        comando_evento("Garcia anuncia a Diego", {  duracion:{m:0,s:38}, comandos:[
          estar('Licenciado'),estar('Monasterio'),estar('Sargento Garcia')
        ]} ),
        comando_evento("Don Nacho pasa prisionero por la plaza", { duracion:{m:3,s:24}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Licenciado'),estar('Monasterio'),estar('Don Nacho'),estar('Sargento Garcia'),estar('Alcalde'),
        ] }),
        comando_evento("Diego en la oficina de monasterio", { duracion:{m:1,s:18}, comandos:[
          estar('Diego de la Vega'),estar('Monasterio'),estar('Sargento Garcia'),
        ] } ),
        comando_evento("Llegada a la Hacienda", { desvio:{m:45}, duracion:{m:0,s:44}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Pepito'),estar('Juan'),estar('Alejandro de la Vega'),
        ] } ),
        comando_evento("Charla entre Diego y su padre", { desvio:{h:1, m:30}, duracion:{m:1,s:31}, comandos:[
          estar('Diego de la Vega'),estar('Alejandro de la Vega'),
        ]} ),
        comando_evento("Diego y Bernardo inventan al Zorro", { desvio:{m:15}, duracion:{m:0,s:37}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),
        ]} ),
        comando_evento("Bernardo conoce a Tornado", { desvio:{h:2}, duracion:{m:1,s:20}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Tornado'),
        ]} ),
        comando_evento("Monasterio planea matar a Don Nacho", { desvio:{h:7,m:5}, duracion:{m:1,s:9}, comandos:[
          estar('Licenciado'),estar('Monasterio'),estar('Don Nacho'),estar('Sargento Garcia'),estar('Tornado'),
        ] } ),
        comando_evento("El Zorro se infiltra en el cuartel", {  duracion:{m:1,s:34}, comandos:[
          estar('Diego de la Vega'),estar('Sargento Garcia'),estar('Don Nacho'),
        ] } ),
        comando_evento("Monasterio ordena a Garcia ir a dormir", {  duracion:{m:0,s:28}, comandos:[
          estar('Monasterio'),estar('Sargento Garcia'),
        ] } ),
        comando_evento("El Zorro habla con Don Nacho", {  duracion:{m:0,s:26}, comandos:[
          estar('Diego de la Vega'),estar('Don Nacho'),
        ] } ),
        comando_evento("Monasterio conspira con el Licenciado", {  duracion:{m:0,s:22}, comandos:[
          estar('Licenciado'),estar('Monasterio'),
        ] } ),
        comando_evento("El Zorro asalta a Garcia", { duracion:{m:0,s:40}, comandos:[
          estar('Diego de la Vega'),estar('Sargento Garcia'),
        ] } ),
        comando_evento("El Zorro libera a Don Nacho", { duracion:{m:3,s:40}, comandos:[
          estar('Diego de la Vega'),estar('Licenciado'),estar('Monasterio'),estar('Don Nacho'),estar('Sargento Garcia'),estar('Tornado'),
        ] } ),
        comando_evento("El Zorro se despide de Don Nacho", {desvio: {m:5}, duracion:{m:1,s:29}, comandos:[
          estar('Diego de la Vega'),estar('Don Nacho'),estar('Tornado'),
        ] } ),
      ]}),
      comando_evento("El pasadizo secreto del Zorro", {origen: moment('1820-01-02'), comandos: [
        comando_evento("Garcia y un lancero pegan carteles", {desvio: {h:17}, duracion:{m:1, s:10}, comandos:[
          estar('Lancero1', {edad:30})
        ]}),
        comando_evento("Garcia informa a Monasterio", {duracion:{s:50}, comandos:[
          estar('Monasterio'),estar('Sargento Garcia'),
        ]}),
        comando_evento("Diego le muestra a Bernardo el pasadizo", {duracion:{m:3, s:38}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Tornado'),
        ]}),
        comando_evento("Diego visita la Abadia", {desvio: {h:1},duracion:{m:1, s:10}, comandos:[
          estar('Diego de la Vega'),estar('Padre Felipe', {edad:45}), estar('Don Nacho'),
        ]}),
        comando_evento("Don Nacho explica que va a ver al Governador", {desvio: {m:1},duracion:{s:40}, comandos:[
          estar('Diego de la Vega'),estar('Padre Felipe'), estar('Don Nacho'),
        ]}),
        comando_evento("Diego visita la Hacienda de Don Nacho", {desvio: {h:1},duracion:{s:50}, comandos:[
          estar('Diego de la Vega'),estar('Elena Torres', {edad:20}),
        ]}),
        comando_evento("Diego encuentra a Monasterio, que se lleva a Benito", {desvio: {h:1},duracion:{m:6}, comandos:[
          estar('Diego de la Vega'),estar('Pepito'),estar('Monasterio'),estar('Sargento Garcia'),estar('Benito Avalon', {edad:25})
        ]}),
        comando_evento("Diego sale a rescatar a Benito", {duracion:{ m:5}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Tornado'),
        ]}),
        comando_evento("En la hacienda Torres Monasterio lucha contra Benito", {desvio: {h:1}, duracion:{ m:2}, comandos:[
          estar('Elena Torres'),estar('Benito Avalon'),estar('Sargento Garcia'),estar('Monasterio')
        ]}),
        comando_evento("El Zorro llega al rescate", {duracion:{ m:4, s:30}, comandos:[
          estar('Elena Torres'),estar('Benito Avalon'),estar('Sargento Garcia'),estar('Monasterio'),estar('Diego de la Vega')
        ]}),
        comando_evento("Diego vuelve a su casa", {desvio: {h:1}, duracion:{ m:5}, comandos:[
          estar('Diego de la Vega'),estar('Bernardo'),estar('Sargento Garcia'),estar('Monasterio')
        ]}),
      ]})
  
    ]}).aplicar()
  })

  it('entonces tiene que andar bien', () => {
    console.log()
    console.table(m.entorno.tabla_eventos())
    console.table(m.entorno.tabla_personas())
  })
})


