import {expect} from 'chai'
import {m} from 'motor'
import {narre} from '../index.js'

let comando, callback, resultado, valor
describe('Cuando tengo un comando', () => {
  describe('que tiene algo que no es una function como callback', () => {
    beforeEach( () => {
      valor = true
      callback = valor
      comando = m.Comando.crear({callback: callback })
    })
    describe('y lo ejecuto', () => {
      beforeEach( () => resultado = comando.ejecutar() )
      it('entonces debe devolver el valor original del callback', () => {
        expect(resultado).to.be.eq(valor)
      })
      it('entonces debo tener como resultado el valor original del callback', () => {
        expect(comando.resultado).to.be.eq(valor)
      })  
    })
  })
  describe('que tiene algo que es una function como callback', () => {
    beforeEach( () => {
      valor = 'desde dentro del callback'
      callback = () => valor
      comando = m.Comando.crear({callback: callback })
    })
    describe('y lo ejecuto', () => {
      beforeEach( () => resultado = comando.ejecutar() )
      it('entonces debe devolver el resultado de la ejecucion', () => {
        expect(resultado).to.be.eq(valor)
      })
      it('entonces debo tener como resultado el resultado de la ejecucion', () => {
        expect(comando.resultado).to.be.eq(valor)
      })  
    })
  })
})