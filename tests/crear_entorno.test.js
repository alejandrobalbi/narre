import {expect} from 'chai'
import {m} from 'motor'
import {narre} from '../index.js'

let entorno
describe('Cuando creo un Entorno', () => {
  beforeEach( () => entorno = m.Entorno.crear())
  it('entonces debo tener un Entorno', () => {
    expect(entorno).to.be.instanceOf(m.Entorno)
  })
  describe('y le agrego', () => {
    describe('algo que no es un Evento', () => {
      it('entonces debo recibir un error', () => {
        expect(() => entorno.agregar('CACA')).to.throw(Error, 'Se esperaba un Evento')
      })
    })
    describe('y le agrego un Evento', () => {
      beforeEach( () => entorno.agregar(m.Evento.crear()))
      it('entonces el entorno tiene un evento mas', () => {
        expect(entorno.eventos.length).to.be.eq(1)
      })
    })
  })
})

